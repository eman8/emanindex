## EmanIndex

### Features 

This plugin provides a page `yoursite.com/emanindexpage`  which proposes a very simple interface to check which items' metadata have which values and which don't have any.

This new version also provides the possibility to update the values directly from its page, as opposed to visiting the admin page for the given item.

This plugin is provided "as is". 

### Crédits


Plugin réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen). Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/pluginseman#B6)
